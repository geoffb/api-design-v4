# API design v4

## Start the project

- Have docker running
- Have tilt and node installed
- Run `tilt up`

## Stop the project

- Run `tilt down`
